import React, { useState } from 'react';
import { BrowserRouter as Router, Routes, Route, Link } from 'react-router-dom';
import Productos from './components/Productos';
import Clientes from './components/Clientes';
import Ventas from './components/Ventas';
import AgregarAlCarrito from './components/AgregarAlCarrito';
import Detalle from './components/Detalle';

const App = () => {
  const [isMenuOpen, setMenuOpen] = useState(false);

  const toggleMenu = () => {
    setMenuOpen(!isMenuOpen);
  };

  return (
    <div className="container">
      <nav className="navbar navbar-expand-lg navbar-light bg-light">
        <a className="navbar-brand" href="/">
          <img width="50"  src="logo.png" alt="Logo" />
        </a>
        <button
          className="navbar-toggler"
          type="button"
          onClick={toggleMenu}
        >
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className={`collapse navbar-collapse ${isMenuOpen ? 'show' : ''}`} id="navbarNavDropdown">

          <ul className="navbar-nav">
            <li className="nav-item">
              <Link to="/productos" className="nav-link" onClick={toggleMenu}>
                Productos
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/clientes" className="nav-link" onClick={toggleMenu}>
                Clientes
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/ventas" className="nav-link" onClick={toggleMenu}>
                Ventas
              </Link>
            </li>
            <li className="nav-item">
              <Link to="/agregar-al-carrito" className="nav-link" onClick={toggleMenu}>
                Agregar al Carrito
              </Link>
            </li>
          </ul>
        </div>
      </nav>

      <hr />

      <Routes>
        <Route path="/productos" element={<Productos />} />
        <Route path="/clientes" element={<Clientes />} />
        <Route path="/ventas" element={<Ventas />} />
        <Route path="/agregar-al-carrito" element={<AgregarAlCarrito />} />
        <Route path="/detalle/:id" element={<Detalle />} />
      </Routes>
    </div>
  );
};

export default App;
