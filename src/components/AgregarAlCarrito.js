import React, { useState, useEffect } from 'react';
import axios from 'axios';

const AgregarAlCarrito = () => {
  const [productos, setProductos] = useState([]);
  const [carrito, setCarrito] = useState([]);
  const [ventaActual, setVentaActual] = useState([]);
  const [clientes, setClientes] = useState([]);
  const [clienteSeleccionado, setClienteSeleccionado] = useState(null);
  const [searchQuery, setSearchQuery] = useState('');

  useEffect(() => {
    // Obtener la lista de productos desde la API
    axios.get('http://localhost:3000/productos')
      .then((response) => {
        setProductos(response.data);
      })
      .catch((error) => {
        console.error('Error al obtener la lista de productos:', error);
      });

    // Obtener la lista de clientes desde la API
    axios.get('http://localhost:3000/clientes')
      .then((response) => {
        setClientes(response.data);
      })
      .catch((error) => {
        console.error('Error al obtener la lista de clientes:', error);
      });
  }, []);

  const agregarAlCarrito = (producto) => {
    setCarrito([...carrito, producto]);
  };

  const handleComprar = () => {
    // Verificar que haya productos en el carrito antes de realizar la compra
    if (carrito.length === 0) {
      console.log('El carrito está vacío. No se puede realizar la compra.');
      return;
    }

    // Verificar que se haya seleccionado un cliente
    if (!clienteSeleccionado) {
      console.log('Debes seleccionar un cliente para realizar la compra.');
      return;
    }

    // Obtener solo los IDs de los productos seleccionados
    const productosIds = carrito.map((producto) => producto.ID_Producto).join(',');

    // Crear la venta con los IDs de los productos y el cliente seleccionado
    setVentaActual({ productos: productosIds, cliente: clienteSeleccionado });


    fetch('http://localhost:3000/ventas', {
      method: 'POST',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ productos: productosIds, cliente: clienteSeleccionado }),
    })
      .then((response) => response.json())
      .then((data) => {
        alert(data.message); 
        setCarrito([]);
        // Reiniciar el cliente seleccionado
        setClienteSeleccionado(null);
      })
      .catch((error) => {
        console.error('Error al realizar la compra:', error);
              });
  };

  const filteredProductos = productos.filter((producto) => {
    return producto.Nombre.toLowerCase().includes(searchQuery.toLowerCase());
  });

  return (
    <div className="container mt-4">
      <h2 className="mb-3">Lista de Productos</h2>
      <div className="input-group mb-3">
        <input
          type="text"
          className="form-control"
          placeholder="Buscar producto..."
          value={searchQuery}
          onChange={(e) => setSearchQuery(e.target.value)}
        />
      </div>
      <ul className="list-group">
        {filteredProductos.map((producto) => (
          <li key={producto.ID_Producto} className="list-group-item d-flex justify-content-between align-items-center">
             <img src={producto.imagen} alt={producto.Nombre_Producto} height="40" />{producto.Nombre} - Q.{producto.Precio}
            <button onClick={() => agregarAlCarrito(producto)} className="btn btn-primary">Agregar al Carrito</button>
          </li>
        ))}
      </ul>

      <h2 className="mt-4">Carrito de Compras</h2>
      {carrito.length === 0 ? (
        <p>El carrito está vacío</p>
      ) : (
        <ul className="list-group">
          {carrito.map((producto) => (
            <li key={producto.ID_Producto} className="list-group-item">
              {producto.Nombre} - Q.{producto.Precio}
            </li>
          ))}
        </ul>
      )}

      {carrito.length > 0 && (
        <div className="row">
           <div className="mt-4 col-md-12">
          <h2>Seleccionar Cliente</h2>
          <select onChange={(e) => setClienteSeleccionado(e.target.value)} className="form-select mb-3">
            <option value="">Seleccionar cliente...</option>
            {clientes.map((cliente) => (
              <option key={cliente.ID_Cliente} value={cliente.ID_Cliente}>
                {cliente.Nombre} {cliente.Apellido} 
              </option>
            ))}
          </select>
          </div>
             <div className="mt-4 col-md-12">
          <button onClick={handleComprar} className="btn btn-success">Comprar</button>
          </div>
        </div>
      )}
    </div>
  );
};

export default AgregarAlCarrito;
