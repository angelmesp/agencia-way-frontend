import React, { useState, useEffect } from 'react';
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import { Button, Container, Form, Row, Col } from 'react-bootstrap';

const Clientes = () => {
  const [clientes, setClientes] = useState([]);
  const [nuevoCliente, setNuevoCliente] = useState({ Nombre: '', Apellido: '', Email: '' });

  useEffect(() => {
    obtenerClientes();
  }, []);

  const obtenerClientes = async () => {
    try {
      const response = await axios.get('http://localhost:3000/clientes');
      setClientes(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const agregarCliente = async () => {
    try {
      await axios.post('http://localhost:3000/clientes', nuevoCliente);
      obtenerClientes();
      setNuevoCliente({ Nombre: '', Apellido: '', Email: '' });
    } catch (error) {
      console.error(error);
    }
  };

  const eliminarCliente = async (id) => {
    try {
      await axios.delete(`http://localhost:3000/clientes/${id}`);
      obtenerClientes();
    } catch (error) {
      console.error(error);
    }
  };

  // Define the columns for the table
  const columns = [
    {
      dataField: 'ID_Cliente',
      text: 'ID',
    },
    {
      dataField: 'Nombre',
      text: 'Nombre',
    },
    {
      dataField: 'Apellido',
      text: 'Apellido',
    },
    {
      dataField: 'Email',
      text: 'Email',
    },
    {
      dataField: 'actions',
      text: 'Acciones',
      formatter: (cell, row) => (
        <Button variant="danger" size="sm" onClick={() => eliminarCliente(row.ID_Cliente)}>
          Eliminar
        </Button>
      ),
    },
  ];

  return (
    <Container className="mt-4">
      <h2>Clientes</h2>
       <p>Agregar Nuevo Cliente</p>
      <Row className="mb-3">

        <Col md={3}>
          <Form.Control
            type="text"
            placeholder="Nombre del cliente"
            value={nuevoCliente.Nombre}
            onChange={(e) => setNuevoCliente({ ...nuevoCliente, Nombre: e.target.value })}
          />
        </Col>
        <Col md={3}>
          <Form.Control
            type="text"
            placeholder="Apellido del cliente"
            value={nuevoCliente.Apellido}
            onChange={(e) => setNuevoCliente({ ...nuevoCliente, Apellido: e.target.value })}
          />
        </Col>
        <Col md={3}>
          <Form.Control
            type="text"
            placeholder="Email"
            value={nuevoCliente.Email}
            onChange={(e) => setNuevoCliente({ ...nuevoCliente, Email: e.target.value })}
          />
        </Col>
        <Col md={3}>
          <Button onClick={agregarCliente} variant="primary">
            Agregar
          </Button>
        </Col>
      </Row>

      <BootstrapTable keyField="ID_Cliente" data={clientes} columns={columns} />
    </Container>
  );
};

export default Clientes;
