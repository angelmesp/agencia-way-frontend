import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import axios from 'axios';
import { Card, Table } from 'react-bootstrap'; 
const Detalle = () => {
  const { id } = useParams(); // Obtén el ID de la venta de los parámetros de ruta
  const [venta, setVenta] = useState(null);

  useEffect(() => {
    // Obtener el detalle de la venta desde la API
    const obtenerDetalleVenta = async () => {
      try {
        const response = await axios.get(`http://localhost:3000/ventas/${id}`);
        setVenta(response.data);
      } catch (error) {
        console.error('Error al obtener el detalle de la venta:', error);
      }
    };

    obtenerDetalleVenta();
  }, [id]); 
  // Renderizar el detalle de la venta
  return (
    <div>
      <h2>Detalle de la Venta</h2>
      {venta ? (
        <Card>
          <Card.Body>
            <Card.Title>Fecha: {new Date(venta.Fecha).toLocaleDateString()}</Card.Title>
            <Card.Text>
              <b>Datos del cliente:</b><br />
              Cliente: {venta.Nombre} {venta.Apellido}<br />
              Email: {venta.Email}<br />
             
              <b>Detalle:</b><br />
              Monto Total: Q.{venta.Monto_Total}<br /><br />
              <Table striped bordered hover>
                <thead>
                  <tr>
                    <th>Producto</th>
                    <th>Cantidad</th>
                    <th>Precio Unitario</th>
                    <th>Subtotal</th>
                  </tr>
                </thead>
                <tbody>
                  {venta.Detalle.map((producto) => (
                    <tr key={producto.ID_Producto}>
                    <td> <img src={producto.imagen} alt={producto.Nombre_Producto} height="40" /></td>
                      <td>{producto.Nombre_Producto}</td>
                      <td>{producto.Cantidad}</td>
                      <td>Q.{producto.Precio_Unitario}</td>
                      <td>Q.{producto.Subtotal}</td>
                    </tr>
                  ))}
                </tbody>
              </Table>
            </Card.Text>
          </Card.Body>
        </Card>
      ) : (
        <p>Cargando detalle de la venta...</p>
      )}
    </div>
  );
};

export default Detalle;
