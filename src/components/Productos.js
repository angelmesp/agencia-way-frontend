import React, { useState, useEffect } from 'react';
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import paginationFactory from 'react-bootstrap-table2-paginator';
import 'bootstrap/dist/css/bootstrap.min.css';
import { Button, Container, Form, Row, Col } from 'react-bootstrap';

const Productos = () => {
  const [productos, setProductos] = useState([]);
  const [nuevoProducto, setNuevoProducto] = useState({ Nombre: '', Precio: 0, Stock: 0 });
  const [imagenProducto, setImagenProducto] = useState(null);

  useEffect(() => {
    obtenerProductos();
  }, []);

  const obtenerProductos = async () => {
    try {
      const response = await axios.get('http://localhost:3000/productos');
      setProductos(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const agregarProducto = async () => {
    try {
      const formData = new FormData();
      formData.append('Nombre', nuevoProducto.Nombre);
      formData.append('Precio', nuevoProducto.Precio);
      formData.append('Stock', nuevoProducto.Stock);
      formData.append('imagen', imagenProducto);

      await axios.post('http://localhost:3000/productos', formData, {
        headers: {
          'Content-Type': 'multipart/form-data',
        },
      });

      obtenerProductos();
      setNuevoProducto({ Nombre: '', Precio: 0, Stock: 0 });
      setImagenProducto(null);
    } catch (error) {
      console.error(error);
    }
  };

  const eliminarProducto = async (id) => {
    try {
      await axios.delete(`http://localhost:3000/productos/${id}`);
      obtenerProductos();
    } catch (error) {
      console.error(error);
    }
  };

  const columns = [
    {
      dataField: 'ID_Producto',
      text: 'ID',
      sort: true,
    },
    {
      dataField: 'Nombre',
      text: 'Nombre',
      sort: true,
    },
    {
      dataField: 'Precio',
      text: 'Precio',
      formatter: (cell, row) => `Q.${row.Precio} `,
      sort: true,
    },
    {
      dataField: 'Stock',
      text: 'Stock',
      sort: true,
    },
    {
      dataField: 'imagen', 
      text: 'Imagen',
      formatter: (cell, row) => <img src={row.imagen} alt={row.Nombre} style={{ width: '100px' }} />,
    },
    {
      dataField: 'actions',
      text: 'Acciones',
      formatter: (cell, row) => (
        <button onClick={() => eliminarProducto(row.ID_Producto)} className="btn btn-danger btn-sm">
          Eliminar
        </button>
      ),
    },
  ];

  return (
    <div className="container mt-4">
      <h2>Productos</h2>
      <Row className="mb-3">
        <div className="mt-4">
          <p>Agregar Nuevo Producto</p>
          <div className="row">
            <div className="col-md-3">
              <input
                type="text"
                className="form-control"
                placeholder="Nombre del producto"
                value={nuevoProducto.Nombre}
                onChange={(e) => setNuevoProducto({ ...nuevoProducto, Nombre: e.target.value })}
              />
            </div>
            <div className="col-md-3">
              <input
                type="number"
                className="form-control"
                placeholder="Precio"
                value={nuevoProducto.Precio}
                onChange={(e) => setNuevoProducto({ ...nuevoProducto, Precio: e.target.value })}
              />
            </div>
            <div className="col-md-3">
              <input
                type="number"
                className="form-control"
                placeholder="Stock"
                value={nuevoProducto.Stock}
                onChange={(e) => setNuevoProducto({ ...nuevoProducto, Stock: e.target.value })}
              />
            </div>
            <div className="col-md-3">
              <input
                type="file"
                className="form-control"
                onChange={(e) => setImagenProducto(e.target.files[0])}
              />
            </div>
            <div className="col-md-3">
              <button onClick={agregarProducto} className="btn btn-primary">
                Agregar
              </button>
            </div>
          </div>
        </div>
      </Row>
      <div className="table-responsive">
        <BootstrapTable
          keyField="ID_Producto"
          data={productos}
          columns={columns}
          pagination={paginationFactory()}
          striped
          hover
          condensed
        />
      </div>
    </div>
  );
};

export default Productos;
