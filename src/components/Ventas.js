import React, { useState, useEffect } from 'react';
import axios from 'axios';
import BootstrapTable from 'react-bootstrap-table-next';
import 'react-bootstrap-table-next/dist/react-bootstrap-table2.min.css';
import { Link } from 'react-router-dom';

const Ventas = () => {
  const [ventas, setVentas] = useState([]);

  useEffect(() => {
    obtenerVentas();
  }, []);

  const obtenerVentas = async () => {
    try {
      const response = await axios.get('http://localhost:3000/ventas');
      setVentas(response.data);
    } catch (error) {
      console.error(error);
    }
  };

  const columns = [
    {
      dataField: 'ID_Venta',
      text: 'ID Venta',
    },
    {
      dataField: 'Nombre',
      text: 'Cliente',
      formatter: (cell, row) => `${row.Nombre} ${row.Apellido}`,
    },
    {
      dataField: 'Email',
      text: 'Email',
    },
    {
      dataField: 'Fecha',
      text: 'Fecha',
      formatter: (cell, row) => {
        const fecha = new Date(cell);
        const options = { day: 'numeric', month: 'numeric', year: 'numeric' };
        return fecha.toLocaleDateString('es-ES', options);
      },
    },
    {
      dataField: 'actions',
      text: 'Acciones',
      formatter: (cell, row) => (
        <Link to={`/detalle/${row.ID_Venta}`} className="btn btn-success btn-sm">
          Ver detalle
        </Link>
      ),
    },
  ];

  return (
    <div className="container mt-4">
      <h2>Ventas</h2>
      <div className="table-responsive">
        <BootstrapTable keyField="ID_Venta" data={ventas} columns={columns} />
      </div>
    </div>
  );
};

export default Ventas;
